import PauseModeButton from './PauseModeButton'
import { _Params } from './Core'

export default class extends Sprite_Button {
  constructor (x, y, onPress) {
    super()
    this.buttons = []
    this._onPress = onPress
    this._listener = this.processButtonTouching.bind(this)
    document.addEventListener('mousemove', this._listener)
    this.createButtons()
  }

  // #if _MZ
  buttonData () {
    return {
      x: this.x,
      y: this.y
    }
  }

  blockWidth () {
    return this.width
  }

  blockHeight () {
    return this.height
  }

  updateOpacity () {
    this.opacity = 255
  }
  // #endif
  processButtonTouching (event) {
    this.children.forEach(button => button.updateMousePosition(event))
  }

  createButtons () {
    const ceButtons = _Params.commonEventButtons
    const gameButtons = _Params.buttons

    const ceButtonsArray = Object.entries(ceButtons).map(([type, value]) => ({ type: 'common', ...value }))
    const gameButtonsArray = Object.entries(gameButtons).map(([type, value]) => ({ type, ...value }))
    const allButtons = [...ceButtonsArray, ...gameButtonsArray]

    const enabledButtons = allButtons.filter(button => {
      return button.enable && button.image
    })
    const sortedButtons = enabledButtons.sort((a, b) => {
      return a.pos - b.pos
    })

    sortedButtons.forEach((button, index) => {
      let position = index
      if (!button.enable) {
        return
      }

      const spacing = _Params.buttonOptions.container.spacing
      const x = position === 1 ? 0 : position === 2 ? spacing : spacing * (position - 1)
      const spriteButton = new PauseModeButton({
        /* Common events are #s, so ensure type = 'common' */
        type: button.type,
        src: ImageManager.loadSystem(button.image),
        action: this._onPress,
        commonEvent: button.commonEvent,
        menuPosition: position
      })

      this.addChild(spriteButton)
      this.buttons.push(spriteButton)

      spriteButton.bitmap.addLoadListener(() => {
        spriteButton.setPosition(x + spriteButton.width, 0)
        this.resize()

        if (!$gameSystem.isMenuEnabled() && button === 'menu') {
          spriteButton.setDisabled()
        }

        if (AudioManager.isMute() && button === 'audio') {
          spriteButton.setDisabled()
        }

        if (!$gameSystem.isSaveEnabled() && button === 'save') {
          spriteButton.setDisabled()
        }
      })
    })
  }

  setPosition () {
    const opts = _Params.buttonOptions.container
    const x = this._tryEval(opts.containerX)
    const y = this._tryEval(opts.containerY)
    this.x = this.x >= Graphics.width ? x - this.getBounds().width : x
    this.y = this.y >= Graphics.height ? y - this.getBounds().height : y
  }

  resize () {
    this.calculateBounds()
    this.setPosition()
  }

  removeListener () {
    document.removeEventListener('mousemove', this._listener)
  }

  _tryEval (expression) {
    try {
      // eslint-disable-next-line no-eval
      return eval(expression)
    } catch (error) {
      console.error('An error occurred from running an expression', expression)
    }
  }
}
