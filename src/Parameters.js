/*:
 * @pluginname X_PauseMode
 * @plugindesc v1.2.0 - A customizable pause scene, enhancing mobile game-play and increasing accessibility
 * @target MZ
 * @modulename X_PauseMode
 * @required
 * @external
 *
 * @author FeniXEngine Contributors
 * @url https://fenixenginemv.gitlab.io/
 *
 * @param Scene Options
 *
 * @param pauseKey
 * @parent Scene Options
 * @text Pause Key
 * @type Number
 * @desc  The key used to pause the game (Default P key)
 * @type number
 * @default 80
 *
 * @param pausePlaytime
 * @parent Scene Options
 * @text Pause Playtime
 * @type boolean
 * @desc  Whether or not to pause the playtime in the pause the mode scene.
 * @default true
 *
 * @param pauseText
 * @parent Scene Options
 * @text Pause Text
 * @type struct<Element>
 * @desc  The text displayed on screen when paused
 * @default {"enable":"true","text":"Pause Mode","x":"Graphics.width / 2 - 100","y":"Graphics.height / 2 - 90","align":"left","font":"GameFont","fontSize":"50"}
 *
 * @param pauseBackground
 * @text Pause Background
 * @parent Scene Options
 * @desc  The background used for the pause mode scene.
 * @type struct<Background>
 * @default {"blur":"true","darkRect":"true","custom":"","x":"0","y":"0"}
 *
 * @param overrideMenuAccess
 * @text Override Menu Access
 * @parent Scene Options
 * @desc Overriding menu access will open the pause mode scene instead of the menu. The menu can be accessed from the pause mode scene.
 * @type boolean
 * @default false
 * @type Boolean
 *
 *
 * @param Sound Options
 *
 * @param exitEnterSe
 * @text Enter/Exit Sound
 * @parent Sound Options
 * @desc  The sound to play when entering or exiting the pause mode scene.
 * @type struct<SoundEffect>
 * @default {"name":"Flash1","volume":"40","pitch":"100","pan":"0.5"}
 *
 * @param buttonsTouchSound
 * @text Buttons Touch Sound
 * @parent Sound Options
 * @desc  Sound effect when a button is touched
 * @type struct<SoundEffect>
 * @default {"name":"Cursor1","volume":"100","pitch":"100","pan":"0.5"}
 *
 *
 * @param Buttons
 *
 * @param buttonsContainer
 * @text Buttons Container
 * @parent Buttons
 * @type struct<ButtonContainer>
 * @desc  All burtons are in one container, changing settings here affect all buttons.
 * @default {"containerX":"Graphics.width / 2 - 150","containerY":"Graphics.height / 2 - 30","spacing":"75"}
 *
 * @param returnButton
 * @text Return Button
 * @parent Buttons
 * @desc  The return button continues gameplay
 * @type struct<Button>
 * @default {"pos":"0","enable":"true","image":"exitLeft","commonEvent":"0"}
 *
 * @param menuButton
 * @text Menu Button
 * @parent Buttons
 * @type struct<Button>
 * @desc  The menu button will open the main menu
 * @default {"pos":"1","enable":"true","image":"menuList","commonEvent":"0"}
 *
 * @param optionsButton
 * @text Options Button
 * @parent Buttons
 * @type struct<Button>
 * @desc  The options button opens the default options menu
 * @default {"pos":"2","enable":"true","image":"options","commonEvent":"0"}
 *
 * @param audioButton
 * @text Mute Button
 * @parent Buttons
 * @type struct<Button>
 * @desc  The mute button mutes all audio
 * @default {"pos":"3","enable":"true","image":"audio","commonEvent":"0"}
 *
 * @param saveButton
 * @text Save Button
 * @parent Buttons
 * @type struct<Button>
 * @desc  The mute button mutes all audio
 * @default {"pos":"4","enable":"true","image":"save","commonEvent":"0"}
 *
 * @param commonEventButtons
 * @text Common Event Buttons
 * @type struct<Button>[]
 * @desc  An extra common event button.
 * @default ["{\"pos\":\"0\",\"enable\":\"false\",\"image\":\"\",\"commonEvent\":\"0\"}"]
 *
 * @param buttonSpriteFrames
 * @text Button Sprite Frames
 * @parent Buttons
 * @type struct<ButtonFrames>
 * @desc  Basic details about your buttons spritesheet frames.
 * @default {"size":"64","normal":"0","touched":"1","disabled":"2"}
 *
 *
 * @param Addons/Extras
 *
 * @param mapName
 * @text Location
 * @parent Addons/Extras
 * @desc  Add the current map name with an icon.
 * @type struct<Addon>
 * @default {"enable":"true","containerX":"5","containerY":"0","icon":"190","el":"{\"enable\":\"true\",\"text\":\"\",\"x\":\"0\",\"y\":\"0\",\"align\":\"left\",\"font\":\"GameFont\",\"fontSize\":\"26\"}","el1":"","el2":""}
 *
 * @param playtime
 * @text Play Time
 * @parent Addons/Extras
 * @desc  Add the playtime with an icon.
 * @type struct<Addon>
 * @default {"enable":"true","containerX":"5","containerY":"Graphics.height","icon":"220","el":"{\"enable\":\"false\",\"text\":\"Playtime: \",\"x\":\"0\",\"y\":\"0\",\"align\":\"left\",\"font\":\"GameFont\",\"fontSize\":\"26\"}","el1":"","el2":""}
 *
 * @param socialButtons
 * @text Social Media Buttons Addon
 * @parent Addons/Extras
 * @desc  Adds Social Media Buttons - Contains No Elements
 * @type struct<Addon>
 * @default {"enable":"false","containerX":"Graphics.width","containerY":"Graphics.height","icon":"0","el":"{\"x\":\"0\",\"y\":\"0\",\"align\":\"left\",\"font\":\"0\"}","el1":"{\"x\":\"0\",\"y\":\"0\",\"align\":\"left\",\"font\":\"0\"}","el2":"{\"x\":\"0\",\"y\":\"0\",\"align\":\"left\",\"font\":\"0\"}"}
 *
 *
 * @command setLocationName
 * @text Set Location Name
 * @desc Set the name of the location on the pause menu. This will override the default location name.
 *
 * @arg name
 * @text Name
 * @type text
 * @default
 *
 *
 * @command pause
 * @text Pause
 * @desc Pause the gamee and display the pause menu.
 *
 * @arg isBattlePause
 * @text Pause Battle
 * @desc This need to be true if you are in a battle.
 * @type boolean
 * @default false
 *
 * @command disablePause
 * @text Disable Pause
 * @desc Disable access to the pause men, preventing the user from pausing.
 *
 * @arg disabled
 * @text Disabled
 * @desc Set to true to disable access to the pause menu, set to false to enable
 * access to the pause menu again.
 * @type boolean
 * @default true
 *
 *
 *@help
 ------------------------------------------------------------------------------
 # TERMS OF USE

--------------------------------------------------------------------------------
 MIT License -

 * Free for use in any RPG Maker MV game project, commercial or otherwise

 * Credit may go to FeniXEngine Contributors or FeniXEngine

 * Though not required, you may provide a link back to the original source code,
   repository or website.

------------------------------------------------------------------------------
 # Instructions

 ------------------------------------------------------------------------------
 ## About Button Sprite Sheets

 * All buttons require you to use an image, this image should contain 3 frames.

 * Button frames can only be 1 line long and only require 3 frames.
   * Each frame is a button state. Normal, Touched and Disabled.
   * Keep a consistent pattern for all buttons. If  your image is in the order of
   "Normal | Touched | Disabled", make sure all you images are the same way.
 * You can freely adjust the frame size in the plugin parameters and
 choose which frame should be which state.

------------------------------------------------------------------------------
 ## About Add-Ons

 ------------------------------------------------------------------------------
Social Media Buttons add-on requires you to have the corresponding plugin
installed and turned on.

=> Playtime - contains 1 element
  Main: The playtime and prefix

=> Location - contains 1 element
  Main: The location and prefix

=> Social Media Buttons - Contains no elements
  -------------------------------------------------------------------------------
 # Plugin Commands

 ------------------------------------------------------------------------------
  PauseMode Pause [isBattlePause]
 - Open the pause menu. If isBattlePause needs to be true if you are in a battle.

  PauseMode DisablePause
 - Disable access to the pause menu

  PauseMode SetLocationName
 - Set the name of the location in the pause menu. This will override the
   default location name.
*/

/* eslint-disable spaced-comment */

/*~struct~SoundEffect:
  @param name
  @text Name
  @type file
  @dir audio/se/
  @desc The name of the sound effect

  @param volume
  @text Volume
  @type number
  @min 1
  @max 100
  @default 50
  @desc The volume of this sound effect. This controls the loudness and softness of the audio.

  @param pitch
  @text Pitch
  @type number
  @min 1
  @max 200
  @default 100
  @desc The pitch of this sound effect. This controls the highs and lows of the audio.

  @param pan
  @text Pan
  @type number
  @default 0.5
  @decimals 0.1
  @min 0
  @max 1
  @desc The pan of this sound effect.
*/

/*~struct~Addon:
  @param enable
  @text Enable Addon
  @type boolean
  @default false
  @desc Turn on this addon to add it to the pause mode scene

  @param containerX
  @text Container's X Position(eval)
  @default 0
  @desc The x position of the container which holds all of this add ons elements

  @param containerY
  @text Container's Y Position(eval)
  @default 0
  @desc The y position of the container which holds all of this add ons elements

  @param icon
  @text Icon
  @default 0
  @desc If this add-on contain an icon, then input the iconId of the icon you'd like displayed

  @param el
  @text Main Element
  @type struct<Element>
  @default {"x":"0","y":"0","align":"left","font":"0"}
  @desc All add-ons contain one element, these are the options for changing the element within the add-ons container.

  @param el1
  @text Element 2
  @type  struct<Element>
  @default {"x":"0","y":"0","align":"left","font":"0"}
  @desc If this add-on contains a 2nd element, you can change it's options here (See Help For More Information)

  @param el2
  @text Element 3
  @type  struct<Element>
  @default {"x":"0","y":"0","align":"left","font":"0"}
  @desc If this add-on contains a 3rd element, you can change it's options here (See Help For More Information)
*/

/*~struct~Element:
  @param enable
  @text Enable
  @type boolean
  @default
  @desc Hide or show the element.

  @param text
  @text Text
  @default
  @desc If the element contains a customizable text element, you can change it here.

  @param x
  @text X Position
  @type number
  @default 0
  @desc The x position of of this element

  @param y
  @text Y Position
  @type number
  @default 0
  @desc The y position of this element

  @param font
  @text Font Face
  @default GameFont
  @desc The font face for the text within this element

  @param fontSize
  @text Font Size
  @default 24
  @desc The font size for the text within this element
*/

/*~struct~ButtonContainer:
  @param containerX
  @text Container's X Position(eval)
  @default 0
  @desc The x position of the container which holds all of the button elements

  @param containerY
  @text Container's Y Position(eval)
  @default 0
  @desc The y position of the container which holds all of the buttons elements

  @param spacing
  @text Spacing
  @type number
  @default 25
  desc The spacing between all the elements within the container. If applicable. (See Help For More Info)
  */

/*~struct~Button:
  @param pos
  @text Position
  @type number
  @default 0
  @desc The position the element should appear within the button container

  @param enable
  @text Enable
  @type boolean
  @default false
  @desc Choose to enable this button, so it appears on the pause mode scene

  @param image
  @text Button Image
  @type file
  @dir img\system
  @default
  @desc The button-sheet image to use for this button.

  @param commonEvent
  @text Common Event
  @type common_event
  @default 0
  @desc The common event to run when this button is pressed.
*/

/*~struct~ButtonFrames:
  @param size
  @type number
  @text Size Of Button Sheet
  @default 64
  @desc The size of one frame on the button-sheet sprite-sheet

  @param normal
  @text Normal Frame
  @type number
  @default 0
  @desc The frame to use on the button-sheet sprite-sheet when the button is not being touched..

  @param touched
  @text On Touch Frame
  @type number
  @default 1
  @desc The frame to use on the button-sheet sprite-sheet when the button is touched.

  @param disabled
  @type number
  @text Disabled Frame
  @default 2
  @desc The frame to use on the button-sheet sprite-sheet when the button is disabled
  */

/*~struct~Background:
  @param blur
  @type boolean
  @text Blurred Background
  @default true
  @desc Create a blurred background of the previous scene.

  @param darkRect
  @type boolean
  @text Darken Background
  @default true
  @desc Create a darkened transparent sprite to cover the background.

  @param custom
  @type file
  @dir img
  @text Custom Background
  @default
  @desc You can use a custom background instead.

  @param x
  @type number
  @text Custom Background X
  @default 0
  @desc Custom backgrounds x position on the scene.

  @param y
  @type number
  @text Custom Background Y
  @default 0
  @desc Custom backgrounds y position on the scene.
  */
