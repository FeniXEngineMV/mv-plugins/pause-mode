import { _Params } from './Core'
export default class extends Sprite_Button {
  constructor (options, x, y) {
    super()
    this._emitter = new PIXI.utils.EventEmitter()
    this._size = _Params.buttonOptions.frames.size
    this._data = options
    this.menuPosition = options.menuPosition
    this.bitmap = options.src
    this.setClickHandler(options.action)
    this._isTouched = false
    this._mousePos = {}
    this._isEnabled = true
    this.forceSelect = false
    this.initializeFrames()
  }

  type () {
    return this._data.type
  }

  on (event, callback) {
    this._emitter.on(event, callback)
  }

  maxFrames () {
    return 3
  }

  // #if _MZ
  updateOpacity () {
    this.opacity = 255
  }

  buttonData () {
    return {
      x: this.x,
      y: this.y
    }
  }

  blockWidth () {
    return _Params.buttonSpriteFrames.size / this.maxFrames() || 48
  }

  blockHeight () {
    return _Params.buttonSpriteFrames.size / this.maxFrames() || 48
  }

  checkBitmap () {
    if (this.bitmap.isReady() && this.bitmap.width < this.blockWidth() * this.maxFrames()) {
      // Probably MV image is used
      throw new Error('ButtonSet image is too small')
    }
  }

  onClick () {
    if (this._clickHandler) {
      this._clickHandler(this._data.type, this)
    } else {
      Input.virtualClick(this._buttonType)
    }
  }
  // #endif

  initializeFrames () {
    this._touchedFrame = _Params.buttonOptions.frames.touched * this._size
    this._disabledFrame = _Params.buttonOptions.frames.disabled * this._size
    this._normalFrame = _Params.buttonOptions.frames.normal * this._size
    this.setColdFrame(this._normalFrame, 0, this._size, this._size)
    this.setHotFrame(this._touchedFrame, 0, this._size, this._size)
    this.setFrame(this._normalFrame, 0, this._size, this._size)
  }

  callClickHandler () {
    if (this._clickHandler) {
      this._clickHandler(this._data.type, this)
    }
  }

  updateMousePosition (event) {
    this._mousePos.x = event.pageX
    this._mousePos.y = event.pageY
  }

  update () {
    super.update()
  }

  commonEvent () {
    return this._data.commonEvent
  }

  updateFrame (event) {
    let frame = null

    if (this.forceSelect === true && !this._isTouched) {
      AudioManager.playSe(_Params.buttonOptions.sound)
      frame = this._hotFrame
      this._isTouched = true
      this.setFrame(frame.x, frame.y, frame.width, frame.height)
      return
    }

    if (this.forceSelect === false) {
      if (this.isButtonTouching() && !this._isTouched && this._isEnabled) {
        AudioManager.playSe(_Params.buttonOptions.sound)
        frame = this._hotFrame
        this._isTouched = true
        this._emitter.emit('touched', this._data.type, this)
      } else if (this._isTouched && !this.isButtonTouching()) {
        frame = this._coldFrame
        this._isTouched = false
      }

      if (frame && this._isEnabled) {
        this.setFrame(frame.x, frame.y, frame.width, frame.height)
      }
    }
  }

  isButtonTouching () {
    if (!this._mousePos.x || !this._mousePos.y) { return false }
    /* #if _MV
    const touchPos = new Point(this._mousePos.x, this._mousePos.y)
    const localPos = this.worldTransform.applyInverse(touchPos)
    return this.hitTest(localPos.x, localPos.y)
    // #else */
    const touchPos = new Point(TouchInput.x, TouchInput.y)
    const localPos = this.worldTransform.applyInverse(touchPos)
    return this.hitTest(localPos.x, localPos.y)
    // #endif
  }

  hitTest (x, y) {
    const rect = new Rectangle(
      -this.anchor.x * this.width,
      -this.anchor.y * this.height,
      this.width,
      this.height
    )
    return rect.contains(x, y)
  }

  setDisabled () {
    this.setFrame(this._disabledFrame, 0, this._size, this._size)
    this._isEnabled = false
  }

  setEnabled () {
    this.setFrame(this._normalFrame, 0, this._size, this._size)
    this._isEnabled = true
  }

  setForceHotFrame () {
    this.forceSelect = true
  }

  setPosition (x, y) {
    this.x = x
    this.y = y
  }
}
