import { _Params } from './Core'
import Scene_GamePause from './Scene_GamePause'

const isScene = function (scene) {
  return SceneManager._scene.constructor === scene
}

const updateKeypress = function keyPress () {
  if (Input.isTriggered('pause')) {
    if (isScene(Scene_Map) && !$gameTemp.pauseDisabled) {
      SceneManager.push(Scene_GamePause)
    }
  }
}

const Scene_Map_Update = Scene_Map.prototype.update
Scene_Map.prototype.update = function () {
  Scene_Map_Update.call(this)
  updateKeypress()
}

const Scene_Map_updateCallMenu = Scene_Map.prototype.updateCallMenu
Scene_Map.prototype.updateCallMenu = function () {
  if (_Params.overrideMenuAccess && this.isMenuCalled() && !$gameTemp.pauseDisabled) {
    AudioManager.playSe(_Params.exitEnterSe)
    SceneManager.push(Scene_GamePause)
  } else {
    Scene_Map_updateCallMenu.call(this)
  }
}
