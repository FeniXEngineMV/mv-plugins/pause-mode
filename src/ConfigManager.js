Object.defineProperty(ConfigManager, 'isMuted', {
  get: function () {
    return AudioManager.isMute()
  },
  set: function (value) {
    AudioManager._isMuted = value
  },
  configurable: true
})

Object.defineProperty(ConfigManager, 'volumeBeforeMute', {
  get: function () {
    return AudioManager._savedVolume
  },
  set: function (value) {
    AudioManager._savedVolume = value
  },
  configurable: true
})

const ConfigManager_makeData = ConfigManager.makeData
ConfigManager.makeData = function () {
  const config = ConfigManager_makeData.call(this)
  config.isMuted = this.isMuted
  config.volumeBeforeMute = this.volumeBeforeMute
  return config
}

const ConfigManager_applyData = ConfigManager.applyData
ConfigManager.applyData = function (config) {
  ConfigManager_applyData.call(this, config)
  this.isMuted = config.isMuted
  this.volumeBeforeMute = config.volumeBeforeMute
}
