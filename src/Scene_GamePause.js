import { _Params } from './Core'
import { Vortex } from './Vortex'
import ButtonContainer from './ButtonContainer'

export default class Scene_GamePause extends Scene_MenuBase {
  constructor () {
    super()
    this._selectedButtonPosition = -1
    this._timeTextSprite = null
    this._mapTextSprite = null
    this._iconSet = ImageManager.loadSystem('IconSet')
    this._initialPlaytimeDrawn = false

    // For now we just check if Vortex has a scene since we only use Vortex in battles
    this.isBattlePause = null
  }

  iconSize () {
    /* #if _MZ
    return { width: ImageManager.iconWidth, height: ImageManager.iconHeight }
    // #else */
    return { width: Window_Base._iconWidth, height: Window_Base._iconHeight }
    // #endif
  }

  savePlaytime () {
    this.playtimeOnPause = Graphics.frameCount
  }

  restorePlaytime () {
    Graphics.frameCount = this.playtimeOnPause
  }

  backgroundSnapBitmap () {
    if (this.isBattlePause) {
      return SceneManager.snap()
    } else {
      return SceneManager.backgroundBitmap()
    }
  }

  start () {
    super.start()
    this.isBattlePause = Vortex.scene
    this.savePlaytime()
  }

  terminate () {
    super.terminate()
    this._buttonContainer.removeListener()
    ConfigManager.save()
    if (_Params.pausePlaytime) {
      this.restorePlaytime()
    }
  }

  create () {
    this.createPauseBackground()
    this.createInfoContainer()
    this.createButtonContainer()
    this.createPauseText()
    this.createMapName()
    this.createPlaytime()
    this.createSocialMediaButtons()
  }

  createButtonContainer () {
    this._buttonContainer = new ButtonContainer(0, 0, this.onButtonPress.bind(this))
    this._buttonContainer.buttons.forEach(button => {
      button.on('touched', this._onButtonTouched.bind(this))
    })

    if (this.isBattlePause) {
      this._buttonContainer.buttons.forEach(button => {
        if (button.type() === 'menu') {
          button.setDisabled(true)
        }
        if (button.type() === 'save') {
          button.setDisabled(true)
        }
      })
    }
    this.addChild(this._buttonContainer)
  }

  createInfoContainer () {
    this._infoContainer = new PIXI.Container()
    this.addChild(this._infoContainer)
  }

  createSocialMediaButtons () {
    const options = _Params.socialButtons
    if (!options.enable) { return }
    if (typeof window.X_SocialMediaButtons === 'undefined') {
      throw new Error('X Pause Mode: Cannot find plugin "Social Media Buttons"')
    }
    const x = this._tryEval(options.containerX)
    const y = this._tryEval(options.containerY)
    const container = new window.X_SocialMediaButtons.Container(x, y)
    this.addChild(container)
  }

  createPlaytime () {
    const options = _Params.playtime
    if (!options.enable) {
      return
    }
    const element = options.el
    const playtime = $gameSystem.playtimeText()
    const x = this._tryEval(options.containerX) || 0
    const y = this._tryEval(options.containerY) || 0
    const text = element.text
    const font = element.fontSize || 18
    this._timeTextSprite = this.createTextSprite(text + playtime, options.icon, x, y, font)
  }

  createMapName () {
    const options = _Params.mapName
    if (!options.enable) {
      return
    }
    const element = options.el
    const mapName = $gameTemp.pauseSceneLocation || $gameMap.displayName()
    const x = this._tryEval(options.containerX) || 0
    const y = this._tryEval(options.containerY) || 0
    const text = element.text
    const font = element.fontSize || 18
    if (mapName && mapName !== '') {
      this._mapTextSprite = this.createTextSprite(text + mapName, options.icon, x, y, font)
    }
  }

  createPauseText () {
    const options = _Params.pauseText
    if (!options.enable) {
      return
    }
    const x = this._tryEval(options.x)
    const y = this._tryEval(options.y)
    this.createTextSprite(options.text, null, x, y, options.fontSize)
  }

  createTextSprite (text, icon = null, x, y, fontSize) {
    const bitmap = new Bitmap(Graphics.width, Graphics.height)
    bitmap.outlineColor = 'black'
    bitmap.outlineWidth = 2
    this._drawText(bitmap, icon, text, fontSize)

    const textWidth = bitmap.measureTextWidth(text)
    const sprite = new Sprite(bitmap)

    sprite.x = x >= Graphics.width ? Graphics.width - textWidth : x
    sprite.y = y >= Graphics.height ? Graphics.height - fontSize * 2 : y

    this._infoContainer.addChild(sprite)

    return sprite
  }

  _drawText (bitmap, icon, text, fontSize) {
    bitmap.fontSize = fontSize || bitmap.fontSize
    if (icon) {
      const iconSize = this.iconSize()
      this._drawIcon(bitmap, icon, 0, iconSize.height / 2 - 8)
      bitmap.drawText(text, iconSize.width + 5, 0, Graphics.width, 50, 'left')
    } else {
      bitmap.drawText(text, 0, 0, Graphics.width, 50, 'left')
    }
  }

  _drawIcon (bitmap, iconIndex, x, y) {
    const pw = this.iconSize().width
    const ph = this.iconSize().height
    var sx = iconIndex % 16 * pw
    var sy = Math.floor(iconIndex / 16) * ph
    bitmap.blt(this._iconSet, sx, sy, pw, ph, x, y)
  }

  createDarkRect (x, y, width, height) {
    const bitmap = new Bitmap(width, height)
    const sprite = new Sprite(bitmap)
    const color1 = 'rgba(0, 0, 0, 0.6)'
    const color2 = 'rgba(0, 0, 0, 0.3)'
    bitmap.gradientFillRect(x, y, width, height / 2, color2, color1, true)
    bitmap.gradientFillRect(x, y + height / 2, width, height / 2, color1, color2, true)
    this.addChild(sprite)
  }

  createPauseBackground () {
    const options = _Params.pauseBackground
    let customBg = null
    if (options.custom) {
      customBg = ImageManager.loadBitmap('img/', options.custom)
    }
    const sceneSnapBitmap = this.backgroundSnapBitmap()
    const bitmap = options.blur ? sceneSnapBitmap : customBg
    this._pauseSprite = new Sprite(bitmap)
    this.addChild(this._pauseSprite)

    if (options.darkRect) {
      this.createDarkRect(0, 0, Graphics.width, Graphics.height)
    }
  }

  returnToMap () {
    AudioManager.playSe(_Params.exitEnterSe)
    SceneManager.goto(Scene_Map)
  }

  returnToBattle () {
    Vortex.unload(true)
  }

  getButton (position) {
    return this._buttonContainer.buttons.find(button => button.menuPosition === position)
  }

  lastButton () {
    const buttons = this._buttonContainer.buttons
    return buttons.sort((a, b) => b.menuPosition - a.menuPosition)[0]
  }

  findAdjacentButtonPosition (position, direction) {
    let newPosition = position

    if (newPosition === -1) {
      newPosition = 0
    } else {
      while (true) {
        if (direction === 'next') {
          newPosition++
          if (newPosition > this.lastButton().menuPosition) {
            newPosition = 0
          }
        } else if (direction === 'previous') {
          newPosition--
          if (newPosition < 0) {
            newPosition = this.lastButton().menuPosition
          }
        }

        if (newPosition !== position) {
          const button = this.getButton(newPosition)
          if (button) {
            return newPosition
          }
        }
      }
    }

    return newPosition
  }

  selectButton (position) {
    this.getButton(position).forceSelect = true
    this._selectedButtonPosition = position
  }

  deselectButton (position) {
    const button = this.getButton(position)
    if (!button) return
    this.getButton(position).forceSelect = false
  }

  selectedButton () {
    return this.getButton(this._selectedButtonPosition)
  }

  selectPrevButton () {
    let adjacentPosition = this.findAdjacentButtonPosition(this._selectedButtonPosition, 'previous')
    this.deselectButton(this._selectedButtonPosition)
    this.selectButton(adjacentPosition)
  }

  selectNextButton () {
    let adjacentPosition = this.findAdjacentButtonPosition(this._selectedButtonPosition, 'next')
    this.deselectButton(this._selectedButtonPosition)
    this.selectButton(adjacentPosition)
  }

  _onButtonTouched (button) {
    // Reset index and disable force select if button is touched
    if (this.getButton(this._selectedButtonPosition)) {
      this.deselectButton(this._selectedButtonPosition)
      this._selectedButtonPosition = -1
    }
  }

  updateTimeText () {
    if (this._timeTextSprite) {
      const options = _Params.playtime
      if (!this._initialPlaytimeDrawn || !_Params.pausePlaytime) {
        this._timeTextSprite.bitmap.clear()
        const text = `${options.el.text}${$gameSystem.playtimeText()}`
        this._drawText(this._timeTextSprite.bitmap, options.icon, text)
        this._initialPlaytimeDrawn = true
      }
    }
  }

  update () {
    super.update()
    this.updateTimeText()
    if (((Input.isTriggered('menu') || TouchInput.isCancelled()) || Input.isTriggered('pause')) && !this.isBattlePause) {
      this.returnToMap()
    }

    if (Input.isTriggered('left') || Input.isRepeated('left')) {
      this.selectPrevButton()
    }

    if (Input.isTriggered('right') || Input.isRepeated('right')) {
      this.selectNextButton()
    }

    if (Input.isTriggered('ok')) {
      if (this.selectedButton()) {
        this.selectedButton().callClickHandler()
      }
    }
  }

  onButtonPress (type, button) {
    switch (type) {
      case 'menu':
        if (this.isMenuEnabled()) {
          SceneManager.push(Scene_Menu)
        } else {
          SoundManager.playBuzzer()
        }
        break

      case 'audio':
        if (ConfigManager.isMuted) {
          button.setEnabled()
        } else {
          button.setDisabled()
        }
        AudioManager.muteAudio()
        break

      case 'options':
        if (this.isBattlePause) {
          Vortex.push(Scene_Options)
          break
        }
        SceneManager.push(Scene_Options)
        break

      case 'save':
        if ($gameSystem.isSaveEnabled() && !this.isBattlePause) {
          SceneManager.push(Scene_Save)
        } else {
          SoundManager.playBuzzer()
        }
        break

      case 'return':
        if (this.isBattlePause) {
          this.returnToBattle()
        } else {
          this.returnToMap()
        }
        break

      case 'common':
        this.returnToMap()
        setTimeout(() => {
          $gameTemp.reserveCommonEvent(button.commonEvent())
          $gameMap.requestRefresh()
        }, 300)
        break
      default:
        break
    }
  }

  isMenuEnabled () {
    return $gameSystem.isMenuEnabled() && !this.isBattlePause
  }

  _tryEval (expression) {
    try {
      // eslint-disable-next-line no-eval
      return eval(expression)
    } catch (error) {
      console.error('An error occurred from running an expression', expression)
    }
  }
}
