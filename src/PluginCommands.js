import { pluginName } from './Core'
import { Vortex } from './Vortex'
import Scene_GamePause from './Scene_GamePause'

// #if _MZ
PluginManager.registerCommand(pluginName, 'pause', (args) => {
  const isBattlePause = JSON.parse(args.isBattlePause)
  if ($gameTemp.pauseDisabled) {
    return
  }
  if (isBattlePause) {
    Vortex.load(Scene_GamePause)
    Vortex.scene.isBattlePause = true
    return
  }
  SceneManager.push(Scene_GamePause)
})

PluginManager.registerCommand(pluginName, 'setLocationName', (args) => {
  $gameTemp.pauseSceneLocation = args.name
})

PluginManager.registerCommand(pluginName, 'disablePause', (args) => {
  $gameTemp.pauseDisabled = JSON.parse(args.disabled)
})
// #endif

// #if _MV
const Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand
Game_Interpreter.prototype.pluginCommand = function (command, args) {
  if (command.toLowerCase() !== 'pausemode') {
    Game_Interpreter_pluginCommand.call(this)
  }
  const subCommand = args[0]

  if (!subCommand) {
    return
  }

  switch (subCommand.toLowerCase()) {
    case 'pause':
      if ($gameTemp.pauseDisabled) {
        return
      }
      if (args[1] && args[1].toLowerCase() === 'true') {
        Vortex.load(Scene_GamePause)
        Vortex.scene.isBattlePause = true
        return
      }
      SceneManager.push(Scene_GamePause)
      break
    case 'setlocationname':
      const name = args.slice(1).join(' ')
      $gameTemp.pauseSceneLocation = name
      break
    case 'disablepause':
      $gameTemp.pauseDisabled = JSON.parse(args[1])
      break
  }
}
// #endif
