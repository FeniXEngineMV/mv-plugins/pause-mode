import { convertParameters } from 'fenix-tools'

export const pluginName = document.currentScript.src.match(/.+\/(.+).js/)[1]

const rawParameters = PluginManager.parameters(pluginName)

export const _Params = convertParameters(rawParameters)

_Params.buttons = {
  return: _Params.returnButton,
  menu: _Params.menuButton,
  audio: _Params.audioButton,
  options: _Params.optionsButton,
  save: _Params.saveButton
}

_Params.buttonOptions = {
  container: _Params.buttonsContainer,
  frames: _Params.buttonSpriteFrames,
  sound: _Params.buttonsTouchSound
}

Object.assign(Input.keyMapper, {
  [_Params.pauseKey]: 'pause'
})
