import { Vortex } from './Vortex'

// #if _MV
const aliasRenderScene = SceneManager.renderScene
SceneManager.renderScene = function () {
  if (Vortex.scene) {
    return
  }
  aliasRenderScene.call(this)
}

const aliasRequestUpdate = SceneManager.requestUpdate
SceneManager.requestUpdate = function () {
  if (Vortex.scene) {
    return
  }
  aliasRequestUpdate.call(this)
}
// #endif
