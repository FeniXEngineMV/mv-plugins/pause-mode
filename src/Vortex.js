class VortexSceneManager {
  constructor (ticker) {
    this.scene = null
    this.nextScene = null
    this._stack = []
    this._ticker = ticker
    this._ticker.autoStart = true
    this._ticker.add(this.update.bind(this))
    this._mvRender = false
  }

  push (sceneClass) {
    if (this.scene) {
      this._stack.push(this.scene.constructor)
    }
    this.load(sceneClass)
  }

  pop () {
    if (this.scene) {
      this.scene.terminate()
      this.scene = null
    }
    if (this._stack.length > 0) {
      this.load(this._stack.pop())
    }
  }

  load (sceneClass) {
    if (this.scene) {
      this.scene.stop()
      this.scene.terminate()
      this.scene = null
    }
    this.scene = new sceneClass() // eslint-disable-line new-cap
    this.scene.isVortex = true
    this.overridePopScene()
    this.scene.start()
    this.scene.create()
    // #if _MZ
    Graphics.setStage(this.scene)
    // #endif

    // #if _MV
    this._ticker.start()
    this._mvRender = true
    // #endif
  }

  unload (resetDefaultSceneMAnager = true) {
    if (this.scene) {
      this.scene.stop()
      this.scene.terminate()
      this.scene = null
      this._stack = []
      if (resetDefaultSceneMAnager) {
        // #if _MZ
        Graphics.setStage(SceneManager._scene)
        // #endif

        // #if _MV
        this._mvRender = false
        SceneManager.requestUpdate()
        // #endif
      }
    }
  }

  update (delta) {
    if (this._mvRender) {
      SceneManager.updateInputData()
      Graphics.render(this.scene)
    }
    if (this.scene) {
      this.scene.update(delta)
    }
    if (this.nextScene) {
      this.load(this.nextScene)
      this.nextScene = null
    }
  }

  // Overrides default MZ/MV classes popScene methods so we can pop scene in Vortex
  overridePopScene () {
    if (this.scene && typeof this.scene.popScene === 'function') {
      this.scene.popScene = () => {
        this.pop()
      }
    }
  }
}

/* #if _MV
export const Vortex = new VortexSceneManager(PIXI.ticker.shared)
 // #else */
export const Vortex = new VortexSceneManager(PIXI.Ticker.shared)
// #endif
