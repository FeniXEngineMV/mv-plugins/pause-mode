import Scene_GamePause from './Scene_GamePause'
import { Vortex } from './Vortex'

const updateKeypress = function keyPress () {
  if (Input.isTriggered('pause') && !$gameTemp.pauseDisabled) {
    Vortex.load(Scene_GamePause)
    Vortex.scene.isBattlePause = true
  }
}

const Scene_Battle_Update = Scene_Battle.prototype.update
Scene_Battle.prototype.update = function () {
  updateKeypress()
  if (Vortex.scene) {
    return
  }
  Scene_Battle_Update.call(this)
}
