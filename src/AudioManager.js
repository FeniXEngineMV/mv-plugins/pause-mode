AudioManager._isMuted = false

AudioManager._savedVolume = {}

AudioManager.isMute = function () {
  return this._isMuted === true
}

AudioManager.saveVolume = function () {
  this._savedVolume = {
    bgm: AudioManager.bgmVolume,
    bgs: AudioManager.bgsVolume,
    me: AudioManager.meVolume,
    se: AudioManager.seVolume
  }
}

AudioManager.restoreVolume = function () {
  this.bgmVolume = this._savedVolume.bgm || 15
  this.bgsVolume = this._savedVolume.bgs || 15
  this.meVolume = this._savedVolume.me || 15
  this.seVolume = this._savedVolume.se || 15
}

AudioManager.muteAudio = function () {
  if (!this._isMuted) {
    this.saveVolume()
    this.bgmVolume = 0
    this.bgsVolume = 0
    this.meVolume = 0
    this.seVolume = 0
    this._isMuted = true
  } else if (this._isMuted) {
    this.restoreVolume()
    this._isMuted = false
  }
}
